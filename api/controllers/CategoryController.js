/**
 * CategoryController
 *
 * @description :: Server-side logic for managing Categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
"use strict";

module.exports = {

  /**
   * `CategoryController.delete()`
   */
  delete: function(req, res) {
    const categoryId = req.param('id');
    Category.findOne({ id: categoryId }).populate('products').exec(function(err, category) {
      if (err) {
        return res.negotiate(err);
      }
      if (!category) {
        const message = `Category with id = ${categoryId} not found.`;
        return res.badRequest(message);
      }
      if (category.products.length !== 0) {
        const message = `Category "${category.name}" (id = ${categoryId}) is not empty and can't be deleted.`;
        return res.badRequest(message);
      }
      Category.destroy({ id: categoryId }).exec(function(err, categories) {
        if (err) {
          return res.negotiate(err);
        }
        sails.log(`Deleted category "${categories[0].name}" (id = ${categoryId})`);
        return res.ok();
      })
    });
  }
};
